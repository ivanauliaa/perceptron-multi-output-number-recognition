import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<List<Double>> dataTrain = readDataAndConvertToDouble("number-training-baru-lagi.csv");
        List<Double> labelList = getLabelFromDataTrain(dataTrain);

        List<List<Double>> weightsList = getWeightsList(labelList.size(), dataTrain.get(0).size(), 1, -1, 1);
        double learningRate = getRandomNumber(1, 0);
        int epochs = 100;

        // PRINT BEGIN
        System.out.println("Neural Network");
        System.out.println("=== === ===");

        System.out.println("Data Tranining: ");
        for (List<Double> row : dataTrain) {
            for (Double col : row) {
                System.out.print(col + "\t");
            }
            System.out.println();
        }

        System.out.println("Init weights and bias (last index in each row): ");
        for (int i = 0; i < weightsList.size(); i++) {
            for (int j = 0; j < weightsList.get(i).size(); j++) {
                System.out.print(String.format("%,.2f", weightsList.get(i).get(j)) + "\t");
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("Learning rate: " + String.format("%,.2f", learningRate));
        System.out.println("Epochs: " + epochs);
        System.out.println("=== === ===");
        // PRINT END

        weightsList = multiOutput(dataTrain, weightsList, labelList, epochs, learningRate);

        // weights = perceptron(dataTrain, weights, epochs, learningRate);

        // PRINT BEGIN
        System.out.println("FINAL WEIGHTS AND BIAS: ");
        for (int i = 0; i < weightsList.size(); i++) {
            System.out.print("[" + i + "] ");
            for (int j = 0; j < weightsList.get(i).size(); j++) {
                System.out.print(String.format("%,.2f", weightsList.get(i).get(j)) + "\t");
            }
            System.out.println();
        }
        System.out.println();

        // PRINT END

        List<List<Double>> oneHotModel = getOneHotModel(labelList);
        List<Double> result = getPrediction(weightsList, dataTrain, oneHotModel, labelList);

        // PRINT BEGIN
        System.out.println();
        System.out.println("RESULT COMPARISON");
        System.out.println("TRAIN\tRESULT");
        for (int i = 0; i < dataTrain.size(); i++) {
            System.out.println(dataTrain.get(i).get(dataTrain.get(i).size() - 1) + "\t" + result.get(i));
        }
        // PRINT END

        double errorRate = getErrorRate(dataTrain, result, true);
        System.out.println("ERROR RATE: " + String.format("%,.2f", errorRate) + "%");
    }

    /**
     * Baca data dari sebuah file lalu diconvert ke double
     * 
     * @param url file directory
     * @return data in list of list of double
     */
    public static List<List<Double>> readDataAndConvertToDouble(String url) {
        final String COMMA_DELIMITER = ",";
        List<List<Double>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(url))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                Double[] doubleValues = new Double[values.length];
                for (int i = 0; i < values.length; i++) {
                    doubleValues[i] = Double.parseDouble(values[i]);
                }
                records.add(Arrays.asList(doubleValues));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    /**
     * Ambil semua label (unique) dari data train
     * 
     * @param dataTrain
     * @return
     */
    public static List<Double> getLabelFromDataTrain(List<List<Double>> dataTrain) {
        List<Double> labels = new ArrayList<>();
        int labelIndex = dataTrain.get(0).size() - 1;

        for (int i = 0; i < dataTrain.size(); i++) {
            if (i == 0) {
                labels.add(dataTrain.get(i).get(labelIndex));
            } else {
                boolean isExist = false;
                for (int j = 0; j < labels.size(); j++) {
                    if (labels.get(j).equals(dataTrain.get(i).get(labelIndex))) {
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    labels.add(dataTrain.get(i).get(labelIndex));
                }
            }
        }

        return labels;
    }

    /**
     * Dapetin list of weights sebanyak jumlah label, setiap baris weights berisi
     * weight sebanyak jumlah fitur di data training
     * 
     * @param listLength
     * @param weightsLength
     * @param max
     * @param min
     * @param initBias
     * @return
     */
    public static List<List<Double>> getWeightsList(int listLength, int weightsLength, int max, int min,
            double initBias) {
        List<List<Double>> weightsList = new ArrayList<>();

        for (int i = 0; i < listLength; i++) {
            weightsList.add(getRandomWeightsAndBias(weightsLength, max, min, initBias)); // last index adalah bias
        }

        return weightsList;
    }

    /**
     * Dapetin nilai random dengan range max dan min
     * 
     * @param max max range
     * @param min min range
     * @return random double number in range
     */
    public static double getRandomNumber(int max, int min) {
        return new Random().nextDouble() * (max - min) + min;
    }

    /**
     * Dapetin nilai weights secara random
     * 
     * @param length   sesuai banyak fitur
     * @param max      nilai max random
     * @param min      nilai min random
     * @param initBias nilai init bias
     * @return weights (weight per fitur)
     */
    public static List<Double> getRandomWeightsAndBias(int length, int max, int min, double initBias) {
        List<Double> weights = new ArrayList<>();

        for (int i = 0; i < length - 1; i++) {
            weights.add(getRandomNumber(max, min));
        }
        weights.add(initBias);

        return weights;
    }

    /**
     * Perulangan untuk multi output
     * 
     * @param dataTrain
     * @param weightsList
     * @param labelList
     * @param epochs
     * @param learningRate
     * @return
     */
    public static List<List<Double>> multiOutput(List<List<Double>> dataTrain, List<List<Double>> weightsList,
            List<Double> labelList, int epochs, double learningRate) {
        for (int i = 0; i < labelList.size(); i++) {
            List<List<Double>> binaryLabelDataTrain = getBinaryLabelDataTrain(dataTrain, labelList.get(i));
            weightsList.set(i, perceptron(binaryLabelDataTrain, weightsList.get(i), epochs, learningRate));
        }

        return weightsList;
    }

    /**
     * Mendapatkan data training dengan kolom label bernilai biner, jika kolom label
     * sesuai dengan label yg dicari maka akan diassign 1, jika bukan maka akan
     * diassign 0
     * 
     * @param dataTrain
     * @param label
     * @return
     */
    public static List<List<Double>> getBinaryLabelDataTrain(List<List<Double>> dataTrain, double label) {
        List<List<Double>> binaryLabelDataTrain = new ArrayList<>();
        int labelIndex = dataTrain.get(0).size() - 1;

        for (int i = 0; i < dataTrain.size(); i++) {
            List<Double> binaryLabelDataTrainRow = new ArrayList<>();
            for (int j = 0; j < dataTrain.get(i).size(); j++) {
                if (j != labelIndex) {
                    binaryLabelDataTrainRow.add(dataTrain.get(i).get(j));
                } else {
                    if (dataTrain.get(i).get(j).equals(label)) {
                        binaryLabelDataTrainRow.add(1.0);
                    } else {
                        binaryLabelDataTrainRow.add(0.0);
                    }
                }
            }
            binaryLabelDataTrain.add(binaryLabelDataTrainRow);
        }

        return binaryLabelDataTrain;
    }

    /**
     * Mencari weight terbaik dengan perceptron
     * 
     * @param dataTrain    list baris data training
     * @param weights      list weights
     * @param epochs       banyaknya epoch (siklus)
     * @param learningRate miu
     * @return weights terbaik
     */
    public static List<Double> perceptron(List<List<Double>> dataTrain, List<Double> weights, int epochs,
            double learningRate) {
        double globalError;
        int labelIndex = dataTrain.get(0).size() - 1;
        int i = 0;

        do {
            globalError = 0;

            // System.out.println("EPOCH: " + (i + 1));
            // System.out.println("--- --- ---");
            for (int j = 0; j < dataTrain.size(); j++) {
                double output = stepFunction(calculateOutput(weights, dataTrain.get(j)), 0);
                double localError = dataTrain.get(j).get(labelIndex) - output;

                // PRINT BEGIN
                // System.out.println("ITERATION: " + (j + 1));
                // System.out.println();
                // System.out.print("FEATRS");
                // for (int l = 0; l < dataTrain.get(j).size() - 1; l++) {
                // System.out.print("\t");
                // }
                // System.out.print("LABEL\t");
                // System.out.print("WEIGHTS");
                // for (int l = 0; l < weights.size() - 1; l++) {
                // System.out.print("\t");
                // }
                // System.out.print("BIAS\t");
                // System.out.print("OUTPUT\t");
                // System.out.println("LOC_ERR");

                // for (int l = 0; l < dataTrain.get(j).size() - 1; l++) {
                // System.out.print(dataTrain.get(j).get(l) + "\t");
                // }
                // System.out.print(dataTrain.get(j).get(dataTrain.get(j).size() - 1) + "\t");
                // for (int l = 0; l < weights.size() - 1; l++) {
                // System.out.print(String.format("%, .2f", weights.get(l)) + "\t");
                // }
                // System.out.print(String.format("%,.2f", weights.get(weights.size() - 1)) +
                // "\t");
                // System.out.print(String.format("%,.2f", output) + "\t");
                // System.out.println(String.format("%,.2f", localError) + "\t");
                // System.out.println();
                // PRINT END

                if (localError != 0) {
                    for (int k = 0; k < weights.size() - 1; k++) {
                        weights.set(k, weights.get(k) + learningRate * localError * dataTrain.get(j).get(k));
                    }

                    weights.set(weights.size() - 1, learningRate * localError);

                    // PRINT BEGIN
                    // System.out.print("UPDATE WEIGHTS: ");
                    // for (int k = 0; k < weights.size() - 1; k++) {
                    // System.out.print(String.format("%,.2f", weights.get(k)) + "\t");
                    // }
                    // System.out.println();
                    // System.out.println("UPDATE BIAS: " + String.format("%,.2f",
                    // weights.get(weights.size() - 1)));
                    // PRINT END
                }

                globalError += Math.abs(localError);
                // System.out.println("GLOBAL ERROR: " + String.format("%,.2f", globalError));
                // System.out.println("--- --- ---");
            }

            if (globalError == 0 && i < epochs) {
                System.out.println("Stopped in epoch = " + i + " because of globalError = 0");
            }

            i++;
        } while (i < epochs && globalError != 0);

        return weights;
    }

    /**
     * Mencari prediksi output
     * 
     * @param weights      list dari weights
     * @param dataTrainRow list dari fitur
     * @return nilai prediksi output
     */
    public static double calculateOutput(List<Double> weights, List<Double> dataTrainRow) {
        double output = weights.get(weights.size() - 1);

        for (int i = 0; i < dataTrainRow.size() - 1; i++) {
            output += weights.get(i) * dataTrainRow.get(i);
        }

        return output;
    }

    /**
     * Fungsi aktivasi step
     * 
     * @param value
     * @return 1 jika value >= 0, 0 jika value < 0
     */
    public static double stepFunction(double value, double threshold) {
        return value >= threshold ? 1 : 0;
    }

    /**
     * Membuat one hot model untuk setiap label sebagai patokan mencari output label
     * 
     * @param labelList
     * @return
     */
    public static List<List<Double>> getOneHotModel(List<Double> labelList) {
        List<List<Double>> oneHotModel = new ArrayList<>();

        for (int i = 0; i < labelList.size(); i++) {
            List<Double> oneHotModelRow = new ArrayList<>();
            for (int j = 0; j < labelList.size(); j++) {
                oneHotModelRow.add((i == j) ? 1.0 : 0.0);
            }
            oneHotModel.add(oneHotModelRow);
        }

        return oneHotModel;
    }

    /**
     * Mendapatkan hasil prediksi berdasarkan weights dan bias hasil learning
     * 
     * @param weights
     * @param dataTrain
     * @return list hasil prediksi
     */
    public static List<Double> getPrediction(List<List<Double>> weightsList, List<List<Double>> dataTrain,
            List<List<Double>> oneHotModel, List<Double> labelList) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < dataTrain.size(); i++) {
            List<Double> outputs = new ArrayList<>();
            for (int j = 0; j < weightsList.size(); j++) {
                outputs.add(stepFunction(calculateOutput(weightsList.get(j), dataTrain.get(i)), 0));
            }
            result.add(evaluateOutputs(outputs, oneHotModel, labelList));
        }

        return result;
    }

    /**
     * Melakukan evaluasi terhadap list of output hasil uji sebuah baris data
     * training terhadap semua isi list of weights
     * 
     * @param expectedOutputs
     * @param oneHotModel
     * @param labelList
     * @return
     */
    public static double evaluateOutputs(List<Double> expectedOutputs, List<List<Double>> oneHotModel,
            List<Double> labelList) {
        List<Double> errors = new ArrayList<>();

        for (int i = 0; i < oneHotModel.size(); i++) {
            errors.add(0.0);
            for (int j = 0; j < oneHotModel.get(i).size(); j++) {
                if (!oneHotModel.get(i).get(j).equals(expectedOutputs.get(j))) {
                    errors.set(i, errors.get(i) + 1);
                }
            }
        }

        int smallestErrorIndex = -1;
        for (int i = 0; i < errors.size(); i++) {
            if (i == 0) {
                smallestErrorIndex = i;
            } else {
                if (errors.get(i) < errors.get(smallestErrorIndex)) {
                    smallestErrorIndex = i;
                }
            }
        }

        return labelList.get(smallestErrorIndex);
    }

    /**
     * Memperbandingkan result prediksi dengan label data train untuk mencari error
     * 
     * @param dataTrain
     * @param result
     * @param percentageReturn apakah nilai error mau dalam bentuk persen
     * @return nilai error
     */
    public static double getErrorRate(List<List<Double>> dataTrain, List<Double> result, boolean percentageReturn) {
        double errorRate = 0;
        for (int i = 0; i < dataTrain.size(); i++) {
            if (!dataTrain.get(i).get(dataTrain.get(i).size() - 1).equals(result.get(i))) {
                errorRate++;
            }
        }

        errorRate /= result.size();
        return percentageReturn ? errorRate * 100 : errorRate;
    }
}